import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';
import '../../provider/state/auth_state.dart';
import '../../utils/custom_button.dart';
import '../../provider/auth_notifier.dart';

@RoutePage()
class HomePage extends ConsumerWidget {
  const HomePage({super.key});

  Future<void> _checkAuthState(BuildContext context, WidgetRef ref) async {
    final authState = ref.read(authProvider);
    if (authState.state == AuthenticationState.authenticated) {
      AutoRouter.of(context).replace(const DashboardPageRoute());
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final size = MediaQuery.of(context).size;
    final authState = ref.watch(authProvider);

    if (authState.state == AuthenticationState.authenticated) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        AutoRouter.of(context).replace(const DashboardPageRoute());
      });
    }

    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 70, right: 40, left: 40),
                decoration: const BoxDecoration(
                  color: Colors.white,
                ),
                child: const Image(image: AssetImage('assets/goal.jpg')),
              ),
              const SizedBox(height: 60),
              Text(
                'Goal Tracker',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                  color: Colors.grey.shade600,
                ),
              ),
              Text(
                'Start your journey towards achieving your goals today! With Goal Tracker.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: Colors.grey.shade400,
                ),
              ),
              const SizedBox(height: 150),
              SizedBox(
                width: size.width * 0.4,
                child: CustomButton(
                  text: 'Get Started',
                  onPressed: () async {
                    bool isFirstTimeUser = await checkIfFirstTimeUser();

                    if (isFirstTimeUser) {
                      AutoRouter.of(context).push(const RegisterPageRoute());
                    } else {
                      AutoRouter.of(context).push(const LoginPageRoute());
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> checkIfFirstTimeUser() async {
    final storage = FlutterSecureStorage();
    String? userToken = await storage.read(key: 'user_token');

    return userToken == null;
  }
}



