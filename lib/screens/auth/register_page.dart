import 'package:auto_route/auto_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:goal_tracking/routes/routes_imports.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

import '../../provider/auth_notifier.dart';
import '../../utils/style.dart';

final _firebase = FirebaseAuth.instance;

@RoutePage()
class RegisterPage extends ConsumerStatefulWidget {
  const RegisterPage({super.key});

  @override
  ConsumerState<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends ConsumerState<RegisterPage> {
  final _form = GlobalKey<FormState>();
  String? _name, _phoneNumber ;
  // var  = '';
  String _emailAddress = '';
  var _password = '';

  bool _obscureText = true; // Add this line


  void _submit() async{

    final isValid = _form.currentState!.validate();

    if(isValid){
      print("hello there");
      _form.currentState!.save();
      try{
        User? user;
        UserCredential userCredential = await _firebase.createUserWithEmailAndPassword(
            email: _emailAddress, password: _password
        );
        user = userCredential.user;
        await user!.updateDisplayName(_name);
        await user.reload();
        user = _firebase.currentUser;

        ref.read(authProvider.notifier).loginWithEmailandPassword(
            _emailAddress,
            _password,
            context
        );
      }on FirebaseAuthException catch(e){
        // print("%%%%%%%%%%%%");
        print(e.code);
        if(e.code == "email-already-in-use"){
          // print("#############");
          AutoRouter.of(context).push(LoginPageRoute());
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Login to Proceed")));
        }
        else{
          ScaffoldMessenger.of(context).clearSnackBars();
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message!)));

        }
      }
      print("data insert was ok");
    }


  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Form(
          key: _form,
          child: Column(
            children: [
              // const SizedBox(height: 50),

              const Text ("Register", style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),),
              const SizedBox(height: 20,),
              Text("Create an Account to manage your Goal",
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.grey[700],
                ),),
              const SizedBox(height: 30,),
              Column(
                children: <Widget>[
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 2.0),
                    child: TextFormField(
                      decoration:const InputDecoration(
                        labelText: 'Name',
                        labelStyle: labelTextStyle,

                        enabledBorder: enabledBorder,
                      ),
                      style: labelTextStyle,
                      validator: (value) =>
                      value != null && value.isNotEmpty
                          ? null
                          : 'Required',
                      onSaved: (value){
                        _name =  value;
                      },
                    ),
                  ),
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 2.0),
                    child: TextFormField(
                      decoration:const InputDecoration(
                        labelText: 'Phone',
                        labelStyle:  labelTextStyle,
                        enabledBorder: enabledBorder,
                      ),
                      style: labelTextStyle,
                      // controller: _passwordController,
                      validator: (value) {
                        if (value!.isEmpty ||
                            !RegExp(r'^[0-9]{10}$').hasMatch(value)) {
                          return 'Enter a valid 10-digit phone number';
                        }
                        return null;
                      },
                    ),),
                  Padding(padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 2.0),
                    child: TextFormField(
                      decoration:const InputDecoration(
                        labelText: 'Email',
                        labelStyle:  labelTextStyle,
                        enabledBorder: enabledBorder,
                      ),
                      keyboardType: TextInputType.emailAddress,
                      autocorrect: false,
                      style: labelTextStyle,
                      validator: (value) =>
                      value != null && value.isNotEmpty && value.contains('@')
                          ? null
                          : 'Valid Email is Required',
                      onSaved: (value){
                        _emailAddress =  value!;
                      },
                    ),),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 2.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Password',
                        labelStyle: labelTextStyle,
                        enabledBorder: enabledBorder,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: _password.length >= 6 ? Colors.green : Colors.red),
                        ),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.visibility),
                          onPressed: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                        ),
                      ),
                      style: labelTextStyle,
                      obscureText: _obscureText,
                      // controller: _passwordController,
                      validator: (value) {
                        if (value == null || value.trim().length < 6) {
                          return 'Password must be at least 6 characters long.';
                        }
                        return null;
                      },
                      onChanged: (value) {
                        setState(() {
                          _password = value;
                        });
                      },
                      onSaved: (value) {
                        _password = value!;
                      },
                    ),
                  ),

                  const SizedBox(height: 20),

                  MaterialButton(
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    color: Color(0xFF3f37c9),
                    onPressed: _submit,
                    child: const Text('Register'),
                  ),


                  const SizedBox(height: 20.0,),
                  RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.bodyLarge,
                      children: [
                        // TextSpan(text: 'Or '),
                        TextSpan(
                          text: "I have an Account, Login",
                          style: const TextStyle(
                              color: Colors.blue,
                              decoration: TextDecoration.none
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = (){
                            AutoRouter.of(context).push(LoginPageRoute());

                            },
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}