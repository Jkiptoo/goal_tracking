import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

@RoutePage()
class UserGuidePage extends StatelessWidget {
  const UserGuidePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF3f37c9),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            AutoRouter.of(context).push(const DashboardPageRoute());
          },
        ),
        title: const Text(
          'User Guide',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              'User Guide: Goal Tracker App',
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20.0),
            _buildSection(
              title: 'Getting Started',
              content: [
                '1. Login/Register: Upon launching the app, you will be prompted to either log in with your existing account or register as a new user.',
                '2. Dashboard: After logging in, you will land on the dashboard screen, where you can access various features of the app.',
              ],
            ),
            const SizedBox(height: 20.0),
            _buildSection(
              title: 'Managing Goals',
              content: [
                '1. Create a Goal: To create a new goal, navigate to the Goals page by selecting it from the menu or dashboard. Click on the floating action button (+ icon) to create a new goal. Enter the details such as title, start date, End date, etc., and save it.',
                '2. View Goals: On the Goals page, you will see a list of all your goals.',
              ],
            ),
            const SizedBox(height: 20.0),
            _buildSection(
              title: 'Viewing Statistics',
              content: [
                '1. View Start Date vs End Date . : On the Dashboard page, also you can view statistics upon clicking on analytics that show a comparison between your start date and end date for each goal under the Analytics section which is under the goals page. This helps you analyze consistency in achieving your goal.',
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSection({required String title, required List<String> content}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 10.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: content.map((text) => Text(' $text')).toList(),
        ),
      ],
    );
  }
}
