import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

import '../../../provider/auth_notifier.dart';

@RoutePage()
class DashboardPage extends ConsumerStatefulWidget {
  const DashboardPage({super.key});

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends ConsumerState<DashboardPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Stream<QuerySnapshot<Map<String, dynamic>>> getCurrentGoals() {
    CollectionReference<Map<String, dynamic>> collectionReference =
        FirebaseFirestore.instance
            .collection('goals')
            .doc('data')
            .collection('goal-data');

    Query<Map<String, dynamic>> query = collectionReference.where('userId',
        isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the  stream of documents in the sub-collection
    return query.orderBy('start', descending: true).limit(5).snapshots();
  }

  final StreamController<QuerySnapshot<Map<String, dynamic>>> _controller =
      StreamController<QuerySnapshot<Map<String, dynamic>>>();

  int completeGoals = 0;
  Future<int> countCompleteGoals() async{
    QuerySnapshot snapshot = await FirebaseFirestore.instance.collection('goals').doc('data').collection('goal-data').where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid).where('complete', isEqualTo: true).get();
    return snapshot.size;
  }

  int pendingGoals = 0;
  Future<int> countPendingGoals() async{
    QuerySnapshot snapshot = await FirebaseFirestore.instance.collection('goals').doc('data').collection('goal-data').where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid).where('complete', isEqualTo: 'false').get();
    return snapshot.size;
  }

  @override
  void initState() {
    super.initState();
    // Initialize variables
    getCurrentGoals().listen((event) {
      _controller.add(event);
    });
   countCompleteGoals().then((value) {
     setState(() {
       completeGoals=value;
     });
   });
   countPendingGoals().then((value) {
     setState(() {
       pendingGoals = value;
     });
   });
  }

  Widget _buildDrawer(BuildContext context, User? loggedInUser) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            decoration: const BoxDecoration(color: Color(0xFF3f37c9)),
            accountName: Text(
              loggedInUser?.displayName ?? '',
              style: const TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white),
            ),
            accountEmail: Text(
              loggedInUser?.email ?? '',
              style: const TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.white),
            ),
            currentAccountPicture:
                const Image(image: AssetImage('assets/images/icon.jpg')),
          ),
          ListTile(
            leading: const Icon(
              Icons.home,
            ),
            title: const Text('Dashboard'),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.agriculture,
            ),
            title: const Text('Goals'),
            onTap: () {
              AutoRouter.of(context).push(const GoalPageRoute());
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.article,
            ),
            title: const Text('Complete'),
            onTap: () {
              AutoRouter.of(context).push(const CompletedGoalsRoute());
            },
          ),
          ListTile(
            leading: const Icon(
              Icons.ac_unit,
            ),
            title: const Text('Pending'),
            onTap: () {
              AutoRouter.of(context).push(const PendingGoalsRoute());
            },
          ),
          ListTile(
            leading: const Icon(Icons.tour),
            title: const Text('Guide'),
            onTap: () {
              AutoRouter.of(context).push(const UserGuidePageRoute());
            },
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Log Out'),
            onTap: () async {
              await ref.read(authProvider.notifier).logOut().then((value) {
                AutoRouter.of(context).push(const LoginPageRoute());
              });
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final authState = ref.watch(authProvider);
    final loggedInUser = authState.user;


    return Scaffold(
      key: _scaffoldKey,
      drawer: _buildDrawer(context, loggedInUser),
      body: ListView(padding: EdgeInsets.zero, children: [
        Column(children: [
          Container(
            decoration: const BoxDecoration(
                color: Color(0xFF3f37c9),
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(50))),
            child: Column(
              children: [
                const SizedBox(
                  height: 50,
                ),
                ListTile(
                  leading: IconButton(
                    icon: const Icon(
                      Icons.menu,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      _scaffoldKey.currentState!.openDrawer();
                    },
                  ),
                  title: Text(
                    loggedInUser?.email ?? '',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                  subtitle: Text(
                    loggedInUser?.displayName ?? '',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                const SizedBox(
                  height: 30,
                )
              ],
            ),
          ),
          Container(
            // color: Theme.of(context).primaryColor,
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(100))),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.only(topLeft: Radius.circular(200))),
              child: GridView.count(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 2,
                crossAxisSpacing: 40,
                mainAxisSpacing: 30,
                children: [
                  GestureDetector(
                    onTap: () {
                      AutoRouter.of(context).push(const GoalPageRoute());
                      // ignore: avoid_print
                      print('Goals');
                    },
                    child:
                        itemDashboard('Goals', Icons.agriculture, Colors.green),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              offset: const Offset(0, 5),
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(.2),
                              spreadRadius: 2,
                              blurRadius: 5)
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          decoration: const BoxDecoration(),
                          child: const Icon(
                            FontAwesomeIcons.moneyBill,
                            color: Colors.deepOrange,
                            size: 50,
                          ),
                        ),
                        const SizedBox(
                          height: 0,
                        ),
                        Text(
                          'Complete',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Text('$completeGoals'),

                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              offset: const Offset(0, 5),
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(.2),
                              spreadRadius: 2,
                              blurRadius: 5)
                        ]),
                    child: GestureDetector(
                      onTap: () {
                        AutoRouter.of(context).push(const PendingGoalsRoute());
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            decoration: const BoxDecoration(),
                            child: const Icon(
                              Icons.landscape,
                              color: Colors.red,
                              size: 50,
                            ),
                          ),
                          const SizedBox(
                            height: 0,
                          ),
                          Text(
                            'Pending',
                            style: Theme.of(context).textTheme.titleMedium,
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Text('$pendingGoals')
                        ],
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              offset: const Offset(0, 5),
                              color: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(.2),
                              spreadRadius: 2,
                              blurRadius: 5)
                        ]),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          decoration: const BoxDecoration(),
                          child: const Icon(
                            FontAwesomeIcons.moneyBill,
                            color: Colors.deepOrange,
                            size: 50,
                          ),
                        ),
                        const SizedBox(
                          height: 0,
                        ),
                        Text(
                          'Analytics',
                          style: Theme.of(context).textTheme.titleMedium,
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
            stream: getCurrentGoals(),
            builder: (BuildContext context,
                AsyncSnapshot<QuerySnapshot<Map<String, dynamic>>> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }

              if (snapshot.hasError) {
                return Center(
                  child: Text('Error: ${snapshot.error}'),
                );
              }

              final List<Map<String, dynamic>> storeGoals = [];

              snapshot.data?.docs.forEach(
                  (DocumentSnapshot<Map<String, dynamic>> documentSnapshot) {
                final Map<String, dynamic>? data = documentSnapshot.data();

                if (data != null) {
                  // Ensure that the necessary fields exist and have the expected types
                  if (data.containsKey('title') &&
                      data.containsKey('start') &&
                      data.containsKey('end')) {
                    // Perform proper type conversion for each field
                    final String goal = data['title'].toString();
                    final String start = data['start'].toString();
                    final String end = data['end'].toString();

                    // Add the data to the list
                    Map<String, dynamic> a = {
                      ...data,
                      'id': documentSnapshot.id,
                      'title': goal,
                      'start': start,
                      'end': end,
                    };
                    storeGoals.add(a);
                  }
                }
              });

              if (storeGoals.isEmpty) {
                return Column(
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        backgroundColor: const Color(0xFF3f37c9),
                      ),
                      onPressed: () {
                        // Navigate to add data page
                      },
                      child: const Text(
                        "User Guide",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                );
              }

              return SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text(
                          'Recent Goals',
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      for (var i = 0; i < storeGoals.length; i++) ...[
                        SizedBox(
                          height: 70,
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(0.0),
                            child: ListTile(
                              title: Text(
                                storeGoals[i]['title'],
                                style: const TextStyle(
                                  color: Color(0xFF3f37c9),
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              subtitle: Text(
                                "Start: ${storeGoals[i]['start']}",
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey[600],
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              trailing: Text(
                                "End: ${storeGoals[i]['end']}",
                                style: TextStyle(
                                  color: Colors.grey[600],
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ]
                    ],
                  ),
                ),
              );
            },
          ),
          const SizedBox(
            height: 20,
          ),
        ])
      ]),
    );
  }

  itemDashboard(String title, IconData iconData, Color background) => Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 5),
                  color: Theme.of(context).primaryColor.withOpacity(.2),
                  spreadRadius: 2,
                  blurRadius: 5)
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration:
                  BoxDecoration(color: background, shape: BoxShape.circle),
              child: Icon(
                iconData,
                color: Colors.white,
                size: 50,
              ),
            ),
            const SizedBox(
              height: 0,
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      );
}

Stream<QuerySnapshot<Map<String, dynamic>>> getCurrentGoals() {
  CollectionReference<Map<String, dynamic>> collectionReference =
      FirebaseFirestore.instance
          .collection('goals')
          .doc('data')
          .collection('goal-data');

  Query<Map<String, dynamic>> query = collectionReference.where('userId',
      isEqualTo: FirebaseAuth.instance.currentUser?.uid);

  // Return the  stream of documents in the sub-collection
  return query.orderBy('start', descending: true).limit(5).snapshots();
}
