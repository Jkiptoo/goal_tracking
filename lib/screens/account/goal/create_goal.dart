import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';
import 'package:goal_tracking/utils/custom_button.dart';
import 'package:intl/intl.dart';

import '../../../utils/style.dart';

@RoutePage()
class CreateGoal extends StatefulWidget {
  const CreateGoal({super.key});

  @override
  State<CreateGoal> createState() => _CreateGoalState();
}

class _CreateGoalState extends State<CreateGoal> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _startDateController = TextEditingController();
  final _endDateController = TextEditingController();
  final _descController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF3f37c9),
        leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: (){
          AutoRouter.of(context).push(const DashboardPageRoute());
        },),
        title: const Text('Create Goal'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _titleController,
                    validator: (value){
                      if(value!.isEmpty || !RegExp(r'^[a-z A-Z,0-9]+$').hasMatch(value)){
                        return 'Please enter a valid goal';
                      }
                    },
                    decoration: const InputDecoration(
                      labelText: 'Title',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'Start Date',
                      hintText: 'Enter a date',
                      labelStyle: labelTextStyle,
                      suffixIcon: Icon(Icons.calendar_today),
                      enabledBorder: enabledBorder,
                    ),
                    controller: _startDateController,
                    readOnly: true,
                    onTap: () async {
                      // ignore: avoid_print
                      print("hey there----");
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2024),
                          lastDate: DateTime(2070));
                      if (pickedDate != null) {
                        setState(() {
                          // ignore: avoid_print
                          print("am here");
                          _startDateController.text =
                              DateFormat('yyyy-MM-dd').format(pickedDate);
                          // dateInputController.text = DateFormat('yyyy-MM-dd').format(pickedDate);

                          // dateInputController.text = DateFormat('yyyy-MM-dd').format(pickedDate);
                        });
                      }

                    },
                    onSaved: (value) {
                    },
                    validator: (value) =>
                    value != null && value.isNotEmpty
                        ? null
                        : 'Required',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      labelText: 'End Date',
                      hintText: 'Enter a date',
                      labelStyle: labelTextStyle,
                      suffixIcon: Icon(Icons.calendar_today),
                      enabledBorder: enabledBorder,
                    ),
                    controller: _endDateController,
                    readOnly: true,
                    onTap: () async {
                      // ignore: avoid_print
                      print("hey there----");
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2024),
                          lastDate: DateTime(2070));
                      if (pickedDate != null) {
                        setState(() {
                          // ignore: avoid_print
                          print("am here");
                          _endDateController.text =
                              DateFormat('yyyy-MM-dd').format(pickedDate);
                          // dateInputController.text = DateFormat('yyyy-MM-dd').format(pickedDate);

                          // dateInputController.text = DateFormat('yyyy-MM-dd').format(pickedDate);
                        });
                      }

                    },
                    onSaved: (value) {
                    },
                    validator: (value) =>
                    value != null && value.isNotEmpty
                        ? null
                        : 'Required',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: TextFormField(
                    controller: _descController,
                    maxLines: 3,
                    decoration: const InputDecoration(
                      labelText: 'Description(Optional)',
                    ),
                  ),
                ),
                const SizedBox(height: 20,),
                SizedBox(
                  width: size.width * 0.4,
                  child: CustomButton(
                      onPressed: (){
                        if(_formKey.currentState!.validate()){
                          addCreatedGoal().then((value) {
                            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Goal added successfully! 💯%')));
                            _formKey.currentState!.reset();
                            AutoRouter.of(context).push(const GoalPageRoute());
                          });
                        }
                      }, text: 'Save'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<void> addCreatedGoal() async{
    var collectionReference = FirebaseFirestore.instance.collection('goals').doc('data').collection('goal-data').doc();
    var documentReference = collectionReference;

    Map<String, dynamic> goalData ={
      'goal_id': documentReference.id,
      'userId': FirebaseAuth.instance.currentUser!.uid,
      'title': _titleController.text,
      'start': _startDateController.text,
      'end': _endDateController.text,
      'desc': _descController.text,
      'complete': 'false'
    };
    try{
      await collectionReference.set(goalData);
    }catch(error){
      print('Error: $error');
    }
  }
}
