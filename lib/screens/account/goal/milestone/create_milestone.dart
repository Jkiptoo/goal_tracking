import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

@RoutePage()
class CreateMilestone extends StatefulWidget {
  final String id;
  const CreateMilestone({super.key, required this.id});

  @override
  State<CreateMilestone> createState() => _CreateMilestoneState();
}

class _CreateMilestoneState extends State<CreateMilestone> {
  final _formKey = GlobalKey<FormState>();

  final _milestoneController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30,
          ),
          onPressed: () {
            AutoRouter.of(context).push(const DashboardPageRoute());
          },
        ),
        title: const Text(
          'Milestone',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        backgroundColor: const Color(0xFF3f37c9),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
                future: FirebaseFirestore.instance
                    .collection('goals')
                    .doc('data')
                    .collection('goal-data')
                    .get(),
                builder: (_, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else if (!snapshot.hasData || snapshot.data == null) {
                    return const Text('No data available');
                  }

                  // Filter documents by the specific ID
                  var documentSnapshot = snapshot.data!.docs.firstWhere(
                    (doc) => doc.id == widget.id,
                  );

                  if (documentSnapshot == null) {
                    return const Text('No data available');
                  }

                  var data = documentSnapshot.data();
                  var start = data['start'] ?? '';
                  var end = data['end'] ?? '';
                  var goalId = data['goal_id'] ?? '';
                  var complete = data['complete'] ?? '';

                  return Column(
                    children: [
                      if (complete == 'false')
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 25.0, vertical: 20.0),
                              child: TextFormField(
                                controller: _milestoneController,
                                validator: (value) {
                                  if (value == null ||
                                      !RegExp(r'^[a-z A-Z 0-9]+$')
                                          .hasMatch(value)) {
                                    return 'Please enter a valid Milestone';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Milestone',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 25.0, vertical: 12.0),
                              child: TextFormField(
                                onChanged: (val) {
                                  start = val;
                                },
                                initialValue: start,
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please enter a valid date';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  labelText: 'Start Date',
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 25.0, vertical: 12.0),
                              child: TextFormField(
                                onChanged: (val) {
                                  end = val;
                                },
                                initialValue: end,
                                validator: (value) {
                                  if (value == null) {
                                    return 'Please enter a valid date';
                                  }
                                  return null;
                                },
                                decoration: const InputDecoration(
                                  labelText:
                                      'End Date', // Corrected label from 'Start Date' to 'End Date'
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0xFF3f37c9),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                  onPressed: () {},
                                  child: const Text(
                                    'Cancel',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                                const SizedBox(
                                  width: 15,
                                ),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0xFF3f37c9),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      var collectionReference =
                                          FirebaseFirestore.instance
                                              .collection('goals')
                                              .doc('data')
                                              .collection('goal')
                                              .doc();

                                      collectionReference.set({
                                        'goal_id': goalId,
                                        'milestone': _milestoneController.text,
                                        'start': start,
                                        'end': end,
                                        'userId': FirebaseAuth
                                            .instance.currentUser!.uid,
                                        'document_id': collectionReference.id,
                                        'status': 'unchecked'
                                      }).then((value) {
                                        _formKey.currentState!.reset();
                                        AutoRouter.of(context).push(
                                            DisplayMilestoneRoute(id: goalId));
                                      });
                                    }
                                  },
                                  child: const Text(
                                    'Add',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                )
                              ],
                            ),
                          ],
                        )
                      else
                        const Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                  "The goal is complete a milestone can't be added"),
                            ],
                          ),
                        ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
