import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';
import 'package:percent_indicator/percent_indicator.dart';

@RoutePage()
class DisplayMilestone extends StatefulWidget {
  final String id;
  const DisplayMilestone({super.key, required this.id});

  @override
  State<DisplayMilestone> createState() => _DisplayMilestoneState();
}

class _DisplayMilestoneState extends State<DisplayMilestone> {
  late User _user;
  Map<String, bool> _milestoneChecked = {};
  String? _selectedGoalId;
  String? _goalId;

  @override
  void initState() {
    super.initState();
    _user = FirebaseAuth.instance.currentUser!;
    _fetchGoalId();  // Fetch the goal_id on initialization
  }

  Future<void> _fetchGoalId() async {
    try {
      DocumentSnapshot doc = await FirebaseFirestore.instance
          .collection('goals')
          .doc('data')
          .collection('goal-data')
          .doc(widget.id) // Use the id passed to the widget
          .get();

      setState(() {
        _goalId = doc.get('goal_id');
      });
    } catch (e) {
      print('Error fetching goal_id: $e');
    }
  }

  double calculateProgress(Map<String, bool> milestones) {
    if (milestones.isEmpty) return 0.0;
    int totalMilestones = milestones.length;
    int checkedMilestones =
        milestones.values.where((isChecked) => isChecked).length;
    return checkedMilestones / totalMilestones;
  }

  @override
  Widget build(BuildContext context) {
    final fireStore = FirebaseFirestore.instance;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            AutoRouter.of(context).push(const GoalPageRoute());
          },
        ),
        centerTitle: true,
        title: const Text(
          "Milestone",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            color: Colors.white,
          ),
        ),
        backgroundColor: const Color(0xFF3f37c9),
      ),
      body: Container(
        margin: const EdgeInsets.all(10.0),
        child: _goalId == null
            ? const Center(
          child: CircularProgressIndicator(),
        )
            : StreamBuilder<QuerySnapshot>(
          stream: fireStore
              .collection('goals')
              .doc('data')
              .collection('goal')
              .where('userId', isEqualTo: _user.uid)
              .where('goal_id', isEqualTo: _goalId)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            List<QueryDocumentSnapshot> filteredDocs = snapshot.data!.docs;
            _milestoneChecked = {
              for (var doc in filteredDocs)
                doc.id:
                (doc.data() as Map<String, dynamic>)['status'] == 'checked'
            };

            if (filteredDocs.isEmpty) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      'assets/goal.jpg',
                      height: 200,
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'Seems you have no Milestone, please add...',
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              );
            }

            double progress = calculateProgress(_milestoneChecked);

            return Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: filteredDocs.length,
                    itemBuilder: (context, index) {
                      Map<String, dynamic> data =
                      filteredDocs[index].data()! as Map<String, dynamic>;
                      DateTime endDate = DateTime.parse(data['end']);
                      var goalId = data['goal_id'];
                      var mileId = data['document_id'];
                      var isChecked = data['status'] == 'checked';
                      if (endDate.isAfter(DateTime.now()) &&
                          endDate.difference(DateTime.now()).inDays <= 2) {
                        // _showReminderDialog(context, data['title']);
                      }

                      return Container(
                        margin: const EdgeInsets.only(bottom: 10.0),
                        padding: const EdgeInsets.only(bottom: 10.0),
                        decoration: const BoxDecoration(
                          border:
                          Border(bottom: BorderSide(color: Colors.black26)),
                        ),
                        child: ListTile(
                          title: Text(
                            "${data['milestone']}",
                            style: const TextStyle(
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                          subtitle: Text(
                            "Date: (${data['start']}), ${data['end']}",
                          ),
                          trailing: Checkbox(
                            value: isChecked,
                            onChanged: (bool? value) async {
                              setState(() {
                                _milestoneChecked[mileId] = value!;
                              });
                              await fireStore
                                  .collection('goals')
                                  .doc('data')
                                  .collection('goal')
                                  .doc(mileId)
                                  .update({
                                'status': value! ? 'checked' : 'unchecked'
                              });
                            },
                          ),
                          isThreeLine: true,
                          onTap: () {
                            setState(() {
                              _selectedGoalId = goalId;
                            });
                            AutoRouter.of(context)
                                .push(CreateMilestoneRoute(id: goalId));
                            // Handle onTap
                          },
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0), // Adjust padding if needed
                  child: LayoutBuilder(
                    builder: (context, constraints) {
                      // Calculate the width considering the leading and trailing texts
                      double availableWidth = constraints.maxWidth -
                          100; // Estimate width of leading and trailing

                      return Row(
                        children: [
                          const Text(
                            '0%',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          SizedBox(
                              width:
                              10), // Add space between text and progress bar
                          Expanded(
                            child: LinearPercentIndicator(
                              width: availableWidth,
                              percent: progress,
                              animationDuration: 1000,
                              animation: true,
                              lineHeight: 20,
                              progressColor: const Color(0xFF3f37c9),
                              backgroundColor: Colors.grey[300],
                            ),
                          ),
                          SizedBox(
                              width:
                              10), // Add space between progress bar and text
                          const Text(
                            '100%',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                const SizedBox(height: 10),
              ],
            );
          },
        ),
      ),
    );
  }
}
