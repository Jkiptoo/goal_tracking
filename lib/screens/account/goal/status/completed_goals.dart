import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

@RoutePage()
class CompletedGoals extends StatefulWidget {
  const CompletedGoals({super.key});

  @override
  State<CompletedGoals> createState() => _CompletedGoalsState();
}

class _CompletedGoalsState extends State<CompletedGoals> {
  late User _user;

  @override
  void initState() {
    super.initState();
    _user = FirebaseAuth.instance.currentUser!;
  }

  @override
  Widget build(BuildContext context) {
    final fireStore = FirebaseFirestore.instance;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white,),
          onPressed: () {
            AutoRouter.of(context).push(const DashboardPageRoute());
          },
        ),
        centerTitle: true,
        title: const Text(
          "Completed Goals",
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.white
          ),
        ),
        backgroundColor: const Color(0xFF3f37c9),
      ),

      body: _buildFarmList(fireStore, 'own'),

      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF3f37c9),
        onPressed: () {
          AutoRouter.of(context).push(const CreateGoalRoute());
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add, color: Colors.white,),
      ),
    );
  }

  Widget _buildFarmList(FirebaseFirestore fireStore, String ownershipType) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: StreamBuilder<QuerySnapshot>(
        stream: fireStore
            .collection('goals')
            .doc('data')
            .collection('goal-data')
            .where('userId', isEqualTo: _user.uid)
            .where('complete', isEqualTo: true)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          List<QueryDocumentSnapshot> filteredDocs = snapshot.data!.docs;

          if (filteredDocs.isEmpty) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/farm.jpg',
                    height: 200,
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    'Seems you have no Goals, please add...',
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            );
          }

          return ListView.builder(
            itemCount: filteredDocs.length,
            itemBuilder: (context, index) {
              Map<String, dynamic> data = filteredDocs[index].data()! as Map<String, dynamic>;
              return Container(
                margin: const EdgeInsets.only(bottom: 10.0),
                padding: const EdgeInsets.only(bottom: 10.0),
                decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black26)),
                ),
                child: ListTile(
                  title: Text.rich(
                    TextSpan(
                      text: "${data['title']}",
                      style: const TextStyle(
                        fontWeight: FontWeight.w800,
                      ),
                      children: [
                        TextSpan(
                          text: " Date: (${data['start']}), ${data['end']}",
                          style: const TextStyle(
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                  subtitle: Text(data['desc']),
                  isThreeLine: true,
                  onTap: () {
                    // Handle onTap
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
