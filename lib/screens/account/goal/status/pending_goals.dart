import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

@RoutePage()
class PendingGoals extends StatefulWidget {
  const PendingGoals({super.key});

  @override
  State<PendingGoals> createState() => _PendingGoalsState();
}

class _PendingGoalsState extends State<PendingGoals> {
  late User _user;

  @override
  void initState() {
    super.initState();
    _user = FirebaseAuth.instance.currentUser!;
  }

  @override
  Widget build(BuildContext context) {
    final fireStore = FirebaseFirestore.instance;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white,),
          onPressed: () {
            AutoRouter.of(context).push(const DashboardPageRoute());
          },
        ),
        centerTitle: true,
        title: const Text(
          "Pending Goals",
          style: TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.white
          ),
        ),
        backgroundColor: const Color(0xFF3f37c9),
      ),

      body: _buildFarmList(fireStore, 'own'),

      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF3f37c9),
        onPressed: () {
          AutoRouter.of(context).push(const CreateGoalRoute());
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add, color: Colors.white,),
      ),
    );
  }

  Widget _buildFarmList(FirebaseFirestore fireStore, String ownershipType) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: StreamBuilder<QuerySnapshot>(
        stream: fireStore
            .collection('goals')
            .doc('data')
            .collection('goal-data')
            .where('userId', isEqualTo: _user.uid)
            .where('complete', isEqualTo: 'false')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          List<QueryDocumentSnapshot> filteredDocs = snapshot.data!.docs;

          if (filteredDocs.isEmpty) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'assets/images/farm.jpg',
                    height: 200,
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    'Seems you have no Goals, please add...',
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            );
          }

          return ListView.builder(
            itemCount: filteredDocs.length,
            itemBuilder: (context, index) {
              Map<String, dynamic> data = filteredDocs[index].data()! as Map<String, dynamic>;
              String docId = data['goal_id'];
              return Container(
                margin: const EdgeInsets.only(bottom: 10.0),
                padding: const EdgeInsets.only(bottom: 10.0),
                decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black26)),
                ),
                child: ListTile(
                  title: Text.rich(
                    TextSpan(
                      text: "${data['title']}",
                      style: const TextStyle(
                        fontWeight: FontWeight.w800,
                      ),
                      children: [
                        TextSpan(
                          text: " Date: (${data['start']}), ${data['end']}",
                          style: const TextStyle(
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                  subtitle: Text(data['desc']),
                  trailing: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      backgroundColor: const Color(0xFF3f37c9)
                    ),
                    onPressed: (){
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text('Confirmation'),
                            content: const Text(
                              'Are you sure you want to mark this goal as done? '
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop(); // Close the dialog
                                },
                                child: const Text('Cancel'),
                              ),
                              TextButton(
                                onPressed: () async {
                                  print('=======$docId========');
                                  try {
                                    await FirebaseFirestore
                                        .instance
                                        .collection('goals')
                                        .doc('data')
                                        .collection('goal-data')
                                        .doc(docId)
                                        .update({
                                      'complete': true,
                                    });
                                  } catch (e) {
                                    // Handle error
                                    // ignore: avoid_print
                                    print('Error updating document: $e');
                                  }
                                  Navigator.of(context).pop(); // Close the dialog
                                },
                                child: const Text('Close'),
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: const Text('Done',style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),),
                  ),
                  isThreeLine: true,
                  onTap: () {
                    // Handle onTap
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
