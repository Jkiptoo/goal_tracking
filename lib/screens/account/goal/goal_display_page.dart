import 'package:auto_route/auto_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:goal_tracking/routes/routes_imports.gr.dart';

@RoutePage()
class GoalPage extends StatefulWidget {
  const GoalPage({super.key});

  @override
  State<GoalPage> createState() => _GoalPageState();
}

class _GoalPageState extends State<GoalPage> {
  late User _user;

  @override
  void initState() {
    super.initState();
    _user = FirebaseAuth.instance.currentUser!;
  }

  @override
  Widget build(BuildContext context) {
    final fireStore = FirebaseFirestore.instance;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            AutoRouter.of(context).push(const DashboardPageRoute());
          },
        ),
        centerTitle: true,
        title: const Text(
          "Goals",
          style: TextStyle(fontWeight: FontWeight.w600, color: Colors.white),
        ),
        backgroundColor: const Color(0xFF3f37c9),
      ),
      body: _buildGoalList(fireStore),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color(0xFF3f37c9),
        onPressed: () {
          AutoRouter.of(context).push(const CreateGoalRoute());
        },
        tooltip: 'Add Goal',
        child: const Icon(Icons.add, color: Colors.white),
      ),
    );
  }

  Widget _buildGoalList(FirebaseFirestore fireStore) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: StreamBuilder<QuerySnapshot>(
        stream: fireStore
            .collection('goals')
            .doc('data')
            .collection('goal-data')
            .where('userId', isEqualTo: _user.uid)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          List<QueryDocumentSnapshot> goals = snapshot.data!.docs;

          if (goals.isEmpty) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('assets/goal.jpg', height: 200),
                  const SizedBox(height: 20),
                  const Text(
                    'Seems you have no Goals, please add...',
                    style: TextStyle(fontSize: 18),
                  ),
                ],
              ),
            );
          }

          return ListView.builder(
            itemCount: goals.length,
            itemBuilder: (context, index) {
              Map<String, dynamic> data =
                  goals[index].data() as Map<String, dynamic>;
              String goalId = goals[index].id; // Get the document ID
              var collectionReference = FirebaseFirestore.instance.collection('goals').doc('data').collection('goal').doc();
              var mileId = collectionReference.get();

              DateTime endDate = DateTime.parse(data['end']);
              if (endDate.isAfter(DateTime.now()) &&
                  endDate.difference(DateTime.now()).inDays <= 2) {
                // _showReminderDialog(context, data['title']);
              }

              return Container(
                margin: const EdgeInsets.only(bottom: 10.0),
                padding: const EdgeInsets.only(bottom: 10.0),
                decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.black26)),
                ),
                child: ListTile(
                  title: Text.rich(
                    TextSpan(
                      text: "${data['title']}",
                      style: const TextStyle(fontWeight: FontWeight.w800),
                      children: [
                        TextSpan(
                          text: " Date: (${data['start']} - ${data['end']})",
                          style: const TextStyle(fontWeight: FontWeight.normal),
                        ),
                      ],
                    ),
                  ),
                  subtitle: Text(data['desc']),
                  trailing: TextButton(
                    onPressed: () {
                      AutoRouter.of(context).push(DisplayMilestoneRoute(
                        id: goalId,
                      ));
                    },
                    child: const Text('Milestones'),
                  ),
                  isThreeLine: true,
                  onTap: () {
                    AutoRouter.of(context)
                        .push(CreateMilestoneRoute(id: goalId));
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
