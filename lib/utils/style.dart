import 'package:flutter/material.dart';

const labelTextStyle = TextStyle(
    color: Colors.black87,
    fontSize: 17,
    fontFamily: 'AvenirLight'
);

const enabledBorder = UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.grey,
        width: 1.0)
);

const farmColor = Colors.green;
