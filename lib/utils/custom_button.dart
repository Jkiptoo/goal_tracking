import 'package:flutter/material.dart';

import 'colors.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  const CustomButton({
    super.key,
    required this.text,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
          backgroundColor:greenColor,
          minimumSize: const Size(double.infinity,50),
      ),
      child: Text(text, style: TextStyle(
          color: blackColor,
        fontWeight: FontWeight.w600
      ),),
    );
  }
}