import 'package:auto_route/auto_route.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:goal_tracking/provider/state/auth_state.dart';

import '../routes/routes_imports.gr.dart';

class AuthNotifier extends StateNotifier<AuthenticationStateData> {
  AuthNotifier() : super(AuthenticationStateData(state: AuthenticationState.initial)) {
    _checkLoggedInUser();
  }

  Future<void> _checkLoggedInUser() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      state = AuthenticationStateData(state: AuthenticationState.authenticated, user: currentUser);
    } else {
      state = AuthenticationStateData(state: AuthenticationState.initial);
    }
  }

  Future<void> loginWithEmailandPassword(String email, String password, BuildContext context) async {
    state = AuthenticationStateData(state: AuthenticationState.loading);

    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      state = AuthenticationStateData(state: AuthenticationState.authenticated, user: userCredential.user);
      AutoRouter.of(context).push(const DashboardPageRoute());
    } on FirebaseAuthException catch (e) {
      state = AuthenticationStateData(state: AuthenticationState.error, errorMessage: e.message);
    }
  }

  Future<void> logOut() async {
    await FirebaseAuth.instance.signOut();
    state = AuthenticationStateData(state: AuthenticationState.initial, user: null);
  }
}

final authProvider = StateNotifierProvider<AuthNotifier, AuthenticationStateData>(
      (ref) => AuthNotifier(),
);

