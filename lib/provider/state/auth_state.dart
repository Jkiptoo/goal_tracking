import 'package:firebase_auth/firebase_auth.dart';

enum AuthenticationState { initial, loading, authenticated, error }

extension AuthenticationStateX on AuthenticationState {
  bool get isInitial => this == AuthenticationState.initial;
  bool get isLoading => this == AuthenticationState.loading;
  bool get isAuthenticated => this == AuthenticationState.authenticated;
  bool get isError => this == AuthenticationState.error;
}

class AuthenticationStateData {
  final AuthenticationState state;
  final User? user;
  final String? errorMessage;

  AuthenticationStateData({
    required this.state,
    this.user,
    this.errorMessage,
  });
}
