// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i12;
import 'package:flutter/material.dart' as _i13;
import 'package:goal_tracking/screens/account/dash/dashboard.dart' as _i4;
import 'package:goal_tracking/screens/account/goal/create_goal.dart' as _i2;
import 'package:goal_tracking/screens/account/goal/goal_display_page.dart'
    as _i6;
import 'package:goal_tracking/screens/account/goal/milestone/create_milestone.dart'
    as _i3;
import 'package:goal_tracking/screens/account/goal/milestone/display_milestone.dart'
    as _i5;
import 'package:goal_tracking/screens/account/goal/status/completed_goals.dart'
    as _i1;
import 'package:goal_tracking/screens/account/goal/status/pending_goals.dart'
    as _i9;
import 'package:goal_tracking/screens/account/guide/user_guide.dart' as _i11;
import 'package:goal_tracking/screens/auth/home_page.dart' as _i7;
import 'package:goal_tracking/screens/auth/login_page.dart' as _i8;
import 'package:goal_tracking/screens/auth/register_page.dart' as _i10;

abstract class $AppRouter extends _i12.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i12.PageFactory> pagesMap = {
    CompletedGoalsRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.CompletedGoals(),
      );
    },
    CreateGoalRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.CreateGoal(),
      );
    },
    CreateMilestoneRoute.name: (routeData) {
      final args = routeData.argsAs<CreateMilestoneRouteArgs>();
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i3.CreateMilestone(
          key: args.key,
          id: args.id,
        ),
      );
    },
    DashboardPageRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.DashboardPage(),
      );
    },
    DisplayMilestoneRoute.name: (routeData) {
      final args = routeData.argsAs<DisplayMilestoneRouteArgs>();
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i5.DisplayMilestone(
          key: args.key,
          id: args.id,
        ),
      );
    },
    GoalPageRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.GoalPage(),
      );
    },
    HomePageRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i7.HomePage(),
      );
    },
    LoginPageRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i8.LoginPage(),
      );
    },
    PendingGoalsRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i9.PendingGoals(),
      );
    },
    RegisterPageRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i10.RegisterPage(),
      );
    },
    UserGuidePageRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i11.UserGuidePage(),
      );
    },
  };
}

/// generated route for
/// [_i1.CompletedGoals]
class CompletedGoalsRoute extends _i12.PageRouteInfo<void> {
  const CompletedGoalsRoute({List<_i12.PageRouteInfo>? children})
      : super(
          CompletedGoalsRoute.name,
          initialChildren: children,
        );

  static const String name = 'CompletedGoalsRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i2.CreateGoal]
class CreateGoalRoute extends _i12.PageRouteInfo<void> {
  const CreateGoalRoute({List<_i12.PageRouteInfo>? children})
      : super(
          CreateGoalRoute.name,
          initialChildren: children,
        );

  static const String name = 'CreateGoalRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i3.CreateMilestone]
class CreateMilestoneRoute
    extends _i12.PageRouteInfo<CreateMilestoneRouteArgs> {
  CreateMilestoneRoute({
    _i13.Key? key,
    required String id,
    List<_i12.PageRouteInfo>? children,
  }) : super(
          CreateMilestoneRoute.name,
          args: CreateMilestoneRouteArgs(
            key: key,
            id: id,
          ),
          initialChildren: children,
        );

  static const String name = 'CreateMilestoneRoute';

  static const _i12.PageInfo<CreateMilestoneRouteArgs> page =
      _i12.PageInfo<CreateMilestoneRouteArgs>(name);
}

class CreateMilestoneRouteArgs {
  const CreateMilestoneRouteArgs({
    this.key,
    required this.id,
  });

  final _i13.Key? key;

  final String id;

  @override
  String toString() {
    return 'CreateMilestoneRouteArgs{key: $key, id: $id}';
  }
}

/// generated route for
/// [_i4.DashboardPage]
class DashboardPageRoute extends _i12.PageRouteInfo<void> {
  const DashboardPageRoute({List<_i12.PageRouteInfo>? children})
      : super(
          DashboardPageRoute.name,
          initialChildren: children,
        );

  static const String name = 'DashboardPageRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i5.DisplayMilestone]
class DisplayMilestoneRoute
    extends _i12.PageRouteInfo<DisplayMilestoneRouteArgs> {
  DisplayMilestoneRoute({
    _i13.Key? key,
    required String id,
    List<_i12.PageRouteInfo>? children,
  }) : super(
          DisplayMilestoneRoute.name,
          args: DisplayMilestoneRouteArgs(
            key: key,
            id: id,
          ),
          initialChildren: children,
        );

  static const String name = 'DisplayMilestoneRoute';

  static const _i12.PageInfo<DisplayMilestoneRouteArgs> page =
      _i12.PageInfo<DisplayMilestoneRouteArgs>(name);
}

class DisplayMilestoneRouteArgs {
  const DisplayMilestoneRouteArgs({
    this.key,
    required this.id,
  });

  final _i13.Key? key;

  final String id;

  @override
  String toString() {
    return 'DisplayMilestoneRouteArgs{key: $key, id: $id}';
  }
}

/// generated route for
/// [_i6.GoalPage]
class GoalPageRoute extends _i12.PageRouteInfo<void> {
  const GoalPageRoute({List<_i12.PageRouteInfo>? children})
      : super(
          GoalPageRoute.name,
          initialChildren: children,
        );

  static const String name = 'GoalPageRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i7.HomePage]
class HomePageRoute extends _i12.PageRouteInfo<void> {
  const HomePageRoute({List<_i12.PageRouteInfo>? children})
      : super(
          HomePageRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomePageRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i8.LoginPage]
class LoginPageRoute extends _i12.PageRouteInfo<void> {
  const LoginPageRoute({List<_i12.PageRouteInfo>? children})
      : super(
          LoginPageRoute.name,
          initialChildren: children,
        );

  static const String name = 'LoginPageRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i9.PendingGoals]
class PendingGoalsRoute extends _i12.PageRouteInfo<void> {
  const PendingGoalsRoute({List<_i12.PageRouteInfo>? children})
      : super(
          PendingGoalsRoute.name,
          initialChildren: children,
        );

  static const String name = 'PendingGoalsRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i10.RegisterPage]
class RegisterPageRoute extends _i12.PageRouteInfo<void> {
  const RegisterPageRoute({List<_i12.PageRouteInfo>? children})
      : super(
          RegisterPageRoute.name,
          initialChildren: children,
        );

  static const String name = 'RegisterPageRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i11.UserGuidePage]
class UserGuidePageRoute extends _i12.PageRouteInfo<void> {
  const UserGuidePageRoute({List<_i12.PageRouteInfo>? children})
      : super(
          UserGuidePageRoute.name,
          initialChildren: children,
        );

  static const String name = 'UserGuidePageRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}
