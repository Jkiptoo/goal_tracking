part of 'routes_imports.dart';

@AutoRouterConfig(replaceInRouteName: "Route")
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => RouteType.adaptive();

  @override
  List<AutoRoute> get routes => [
        /// routes go here
        AutoRoute(page: HomePageRoute.page, path: '/'),
        AutoRoute(page: DashboardPageRoute.page),
        AutoRoute(page: RegisterPageRoute.page),
        AutoRoute(page: LoginPageRoute.page),
        AutoRoute(page: CreateGoalRoute.page),
        AutoRoute(page: GoalPageRoute.page),
        AutoRoute(page: UserGuidePageRoute.page),
        AutoRoute(page: PendingGoalsRoute.page),
        AutoRoute(page: CompletedGoalsRoute.page),
        AutoRoute(page: DisplayMilestoneRoute.page),
        AutoRoute(page: CreateMilestoneRoute.page),
      ];
}
